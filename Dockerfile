# Create from Linux Alpine image
FROM alpine:3.14

# First update packages
RUN apk update

# Install python, pip and wheel
RUN apk add python3 py3-pip py3-wheel

RUN echo "hello from docker"

CMD ["/bin/ash"]
